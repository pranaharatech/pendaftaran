<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Pendaftaran | Pranahara Teknologi</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <style>
        /* CSS Custom */
    </style>
</head>
    <body>
    @include('sweetalert::alert')
        <nav class="navbar navbar-light bg-light static">
            <div class="container">
                <a href="#" class="navbar-brand">
                    <img src="{{ ('logo/logo_pranahara.png') }}" alt="" width="40" height="35">
                    
                </a>
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                      <a class="nav-link" aria-current="page" href="/register">Pendaftaran Akun</a>
                    </li>
                </ul>
            </div>
        </nav>
        <main>
        @yield('content')
        </main>

        <hr class="featurette-divider">

    <footer class="container">
        <p>&copy; 2020 - 2021 Pranahara Teknologi.
    </footer>
   <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    </body>
    <style>
    /* JS Custom */
    </style>
</html>
