@extends('layouts.auth')

@section('content')
    <div class="container col-xl-12 col-xxl-8 px-4 py-5">
        <div class="row align-items-center g-lg-5 py-1">
            <div class="col-lg-7 text-center text-lg-start">
                <img src="{{ ('logo/logo_pranahara.png') }}" alt="" width="450" height="430">
                
            </div>
            <div class="col-md-10 mx-auto col-lg-5">
                <form action="/register" method="POST" class="p-4 p-md-5 border rounded-3 bg-light">
                    @csrf
                    
                    <div class="form-floating mb-3">
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="floatingInput" placeholder="Masukkan Nama Lengkap" autofocus required value="{{ old('name')}}">
                        <label for="floatingInput">Nama Lengkap</label>
                        @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" name="username" class="form-control @error('username') is-invalid @enderror" id="floatingInput" placeholder="username" required value="{{ old('username')}}">
                        <label for="floatingInput">Username</label>
                        @error('username')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-floating mb-3">
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="floatingInput" placeholder="name@example.com" required value="{{ old('email')}}">
                        <label for="floatingInput">Email address</label>
                        @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-floating mb-3">
                        <input type="password" name="password" class="form-control" id="floatingPassword" placeholder="Password" required>
                        <label for="floatingPassword">Password</label>
                    </div>
                    <button class="w-100 btn btn-lg btn-primary" type="submit">Daftar</button>
                    <div class="mt-3">Sudah punya akun? <a href="">Masuk</a></div>
                    <hr class="my-4">
                    <small class="text-muted">Klik Daftar, anda setuju dengan syarat & ketentuan yang berlaku.</small>
                </form>
            </div>
        </div>
    </div>
@endsection